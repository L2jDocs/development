# Documentation Guidelines

This is a basic guide on how to install/work with L2jServer documentation

:::warning Images notice
Width: 400px\
Height: 200px\
Size: 200KB max\
Type: jpg/png/jpeg/svg/gif
:::

---

## Software required

1. [cmder](https://cmder.net/)
2. [Node.js](https://nodejs.org/en/)
	- 64-bit / Full (with npm)

---

## Installation 

1.	Create a work folder somewhere (e.g. Desktop\vue)
2.	Open cmder
3.	`cd` to that folder 
4.	Execute: 

```sh
npm install vuepress -g
npm install vuepress-theme-yuu -g
npm install vue-click-outside -g
npm install @vuepress/back-to-top -g
npm install vuepress-plugin-zooming -g


git clone https://JohnMayhem@bitbucket.org/L2jDocs/development.git
```

This will install Vuepress and the addons in the `%AppData%\npm\npm-modules` directory.\
\
5. Navigate to the `%AppData%\npm\npm-modules\vuepress-theme-yuu\components\settings` folder\
\
6. Replace **ThemeOptions.vue** with this [file](https://l2jtest.com/ThemeOptions.vue) 


---

## Dev mode

1. Open cmder
2. `cd` to %Your_work_folder%\development\src
3. Execute:
```
vuepress dev
```
:::warning Notice
VuePress will start a hot-reloading development server at localhost:8080

This is just about enoght to make necessary changes and push them via GIT
:::

\
If you wish to test this in your production enviorment, execute:
```
vuepress build
```
After that, navigate to %Your_work_folder%\development\src\ .vuepress\dist \
Folder contents can be copied directly to a webserver's root directory


---

## Markdows Extensions

Like Custom containers: 

**Input**

```
::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger
This is a dangerous warning
:::

::: details
This is a details block, which does not work in IE / Edge
:::
```


**Output**

::: tip
This is a tip
:::

::: warning
This is a warning
:::

::: danger
This is a dangerous warning
:::

::: details
This is a details block, which does not work in IE / Edge
:::


Can be found [HERE ](https://v1.vuepress.vuejs.org/guide/markdown.html)


---

## Directory Structure

VuePress follows the principle of  **"Convention is better than configuration"**. The recommended structure is as follows:

```
.
├── docs
│   ├── .vuepress (Optional)
│   │   ├── components (Optional)
│   │   ├── theme (Optional)
│   │   │   └── Layout.vue
│   │   ├── public (Optional)
│   │   ├── styles (Optional)
│   │   │   ├── index.styl
│   │   │   └── palette.styl
│   │   ├── templates (Optional, Danger Zone)
│   │   │   ├── dev.html
│   │   │   └── ssr.html
│   │   ├── config.js (Optional)
│   │   └── enhanceApp.js (Optional)
│   │ 
│   ├── README.md
│   ├── guide
│   │   └── README.md
│   └── config.md
│ 
└── package.json


```

Note

Please note the capitalization of the directory name.

-   `docs/.vuepress`: Used to store global configuration, components, static resources, etc.
-   `docs/.vuepress/components`: The Vue components in this directory will be automatically registered as global components.
-   `docs/.vuepress/theme`: Used to store local theme.
-   `docs/.vuepress/styles`: Stores style related files.
-   `docs/.vuepress/styles/index.styl`: Automatically applied global style files, generated at the ending of the CSS file, have a higher priority than the default style.
-   `docs/.vuepress/styles/palette.styl`: The palette is used to override the default color constants and to set the color constants of Stylus.
-   `docs/.vuepress/public`: Static resource directory.
-   `docs/.vuepress/templates`: Store HTML template files.
-   `docs/.vuepress/templates/dev.html`: HTML template file for development environment.
-   `docs/.vuepress/templates/ssr.html`: Vue SSR based HTML template file in the built time.
-   `docs/.vuepress/config.js`: Entry file of configuration, can also be  `yml`  or  `toml`.
-   `docs/.vuepress/enhanceApp.js`: App level enhancement.

Note

When customizing  `templates/ssr.html`, or  `templates/dev.html`, it’s best to edit it on the basis of the  [default template files](https://github.com/vuejs/vuepress/blob/master/packages/%40vuepress/core/lib/client/index.dev.html), otherwise it may cause a build failure.

## Default Page Routing

Here we use the  `docs`  directory as the  `targetDir`  (see  [Command-line Interface](https://v1.vuepress.vuejs.org/api/cli.html#usage)). All the "Relative Paths" below are relative to the  `docs`  directory. Add  `scripts`  in  `package.json`  which is located in your project’s root directory:

```
{
  "scripts": {
    "dev": "vuepress dev docs",
    "build": "vuepress build docs"
  }
}

```

For the above directory structure, the default page routing paths are as follows:

| Relative Path | Page Routing |
|--|--|
| `/README.md` | `/` |
| `/guide/README.md` | `/guide/` |
| `/config.md` | `/config.html` |



