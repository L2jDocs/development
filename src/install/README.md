# Getting Started

Before we can jump head first into making an L2J based server, we need to go over a few things.

---

## Prerequisites

- _**[CRUCIAL]**_    Time and patience.
    -   Building, configuring and managing a L2J Server is easier than you might think, but keep in mind, that any good result will require time and patience.


-  _**[CRUCIAL]**_  **KNOW YOUR HARDWARE**
    -   Your CPU  name and generation, it's capabilities
    -   Your storage devices (HDD/SSD, NVMe/AHCI/RAID/IDE configurations)
    -   Your  **Network Address** and it's  configuration

-  _**[CRUCIAL]**_  **A BASIC KNOWLEDGE OF COMMAND LINES AND HOW TO USE A TERMINAL/COMMAND PROMPT**
    -   This is not just [CRUCIAL], this is the basis of this whole guide. We can't help you if you don't know how to  `cd`  to a directory or delete a file.


---

## Hardware requirements

- **Entry**: 
	-   Any 64-bit capable CPU
	-   2GB RAM if you're planning to use L2JServer for a localhost/LAN setup to get acquainted with the process
		- *Note: Only in case the game client is not run on the same machine*
	-   256MB Disk Space for the server installation 
	
- **Performance**: 
    -   Any Quad-core 64-bit capable CPU 
    -   8GB RAM for small-medium production server 
    -   1GB Disk Space for the server installation with Geodata
- **Scalable**: 
    -   Multisocket Server systems or Cloud-based solutions
    -   32GB+ [ECC](https://en.wikipedia.org/wiki/ECC_memory) RAM for a high demand installation
    -   20GB+ SSD or [NVMe](https://en.wikipedia.org/wiki/NVM_Express) storage with high [IOPS](https://en.wikipedia.org/wiki/IOPS) performance for an extensively loaded database 
	

---

## Network requirements


- Entry: 
	-	5-10 mb/s
    -   Optional for localhost deployment
        - *Note: a LAN connection is required for LAN Play*
- Performance: 
    -   50 mb/s for ~ 100 Players 
- Scalable: 
    -   1 GB/s asynchronous and higher



---

## OS Specifications

	
|                    | Windows                                  | Linux                           | macOS |
| -------------      |:-------------                            |:-----                           |:-----|
| **Recommended**    | [Windows 10 (64-bit)](/windows/)         | [CentOS 8.1](/linux/)           | [macOS 10.15.7](/macos/) |
| Will work on *     | Windows 8.x (Desktop)(64-bit)            | CentOS 7.6 (64-bit) and above   | macOS 11 |
|                    | Windows 7 SP1 (64-bit)                   | Debian 10                       | macOS 10.14 |
|                    | Windows Vista SP2 (64-bit)               | RHEL 7.x (64-bit) and above     | |
|                    | Windows Server 2008 R2 SP1 (64-bit)      | Ubuntu 16.04 (64-bit) and above | |
|                    | Windows Server 2012 R2 (64-bit)          | | |
|                    | Windows Server 2019 and Higher (64-bit)  | | |
	
**Note: Not tested*	
	
---

::: warning Important
-   Hypervisor setup/configuration will not be covered by this documentation 
:::
