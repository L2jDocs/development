# Setup L2JServer - Ubuntu Server 20.04

## Install required packages

sudo apt-get install -y git  
sudo apt-get install -y openjdk-14-jre-headless  
sudo apt-get install -y mariadb-server

sudo apt-get install -y mariadb-client

sudo apt-get install -y unzip

## Enable connections from outside of the host

sudo nano /etc/mysql/mariadb.conf.d/50-server.cnf

  

bind-address = 0.0.0.0

## Restart the DataBase

sudo systemctl restart mariadb

## Initialize the DataBase

sudo mysql_secure_installation

## Create l2j user in the DataBase

sudo mariadb -u root -p  
CREATE OR REPLACE USER 'l2j'@'%' IDENTIFIED BY 'l2jserver2019';  
GRANT  ALL PRIVILEGES ON *.* TO 'l2j'@'%' IDENTIFIED BY 'l2jserver2019';  
FLUSH PRIVILEGES;  
quit;

## Clone Bitbucket repos

git clone -b master https://yeiij@bitbucket.org/l2jserver/l2j-server-cli.git  
git clone -b master https://bitbucket.org/l2jserver/l2j-server-login.git  
git clone -b develop https://bitbucket.org/l2jserver/l2j-server-game.git  
git clone -b develop https://bitbucket.org/l2jserver/l2j-server-datapack.git  
git clone -b develop https://bitbucket.org/l2jserver/l2j-server-datapack.git

## Give permissions to the compilation script

sudo chmod 755 l2j-server-cli/mvnw

sudo chmod 755 l2j-server-login/mvnw

sudo chmod 755 l2j-server-game/mvnw

sudo chmod 755 l2j-server-datapack/mvnw

## Compile projects

cd ~/l2j-server-cli && ./mvnw install

cd ~/l2j-server-login && ./mvnw install

cd ~/l2j-server-game && ./mvnw install

cd ~/l2j-server-datapack && ./mvnw install

## Create the L2JServer folder and subfolders

cd ~  
mkdir ~/l2jserver

mkdir ~/l2jserver/cli  
mkdir ~/l2jserver/login  
mkdir ~/l2jserver/game

## Unzip and fit packages together

unzip -qq ~/l2j-server-cli/target/l2jcli-*.zip -d ~/l2jserver/cli  
unzip -qq ~/l2j-server-login/target/l2j-server-login-*.zip -d ~/l2jserver/login  
unzip -qq ~/l2j-server-game/target/l2j-server-game-*.zip -d ~/l2jserver/game  
unzip -qq ~/l2j-server-datapack/target/l2j-server-datapack-*.zip -d ~/l2jserver/game

## Install tables

java -jar ~/l2jserver/cli/l2jcli.jar  
db install -sql l2jserver/login/sql/ -u l2j -p l2jserver2019 -m FULL -t LOGIN -c -mods  
db install -sql l2jserver/game/sql -u l2j -p l2jserver2019 -m FULL -t GAME -c -mods

## Create the log folders

cd ~  
mkdir ~/l2jserver/login/log  
mkdir ~/l2jserver/game/log

## Set starters permissions

chmod 755 ~/l2jserver/login/LoginServer_loop.sh  
chmod 755 ~/l2jserver/login/startLoginServer.sh  
  
chmod 755 ~/l2jserver/game/GameServer_loop.sh  
chmod 755 ~/l2jserver/game/startGameServer.sh

  

## Run Login

cd ~/l2jserver/login && java -jar l2jlogin.jar

  

## Run Game

cd ~/l2jserver/game && java -jar l2jserver.jar

  

## Done.