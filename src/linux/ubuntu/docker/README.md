
L2JServer on Raspberry Pi 4 4Gb + Ubuntu Server 20.04 LTS 64Bits

  

# Setup the OS

  
  

## Install required packages

  
  

Ubuntu 20.04 LTS 64Bits - Raspberry Pi 4 - 4GB RAM

Those steps works almost the same for RaspiOS and DietPi

User: ubuntu # "pi" for RaspiOS "root" for DietPi  
Password: ubuntu # "raspberry" for RaspiOS "dietpi" for DietPi

  

Configure keyboard layout

sudo dpkg-reconfigure keyboard-configuration  
sudo loadkeys es

  

Setup Time Zone

sudo timedatectl set-timezone Europe/Madrid

  

Install required packages

sudo apt-get update  
sudo apt-get upgrade -y  
sudo apt-get install -y wpasupplicant  
sudo apt-get install -y openssh-server  
sudo apt-get install -y ufw  
sudo apt-get install -y fail2ban  
sudo apt-get install -y docker.io  
sudo apt-get install -y docker-compose

  
  

## Configure Docker

  
  

Enable docker daemon

sudo systemctl enable --now docker

  

Give permissions to docker

sudo usermod -aG docker $USER

  

## Configure Firewall

  
  

Remove IPV6 from firewall config

sudo nano /etc/default/ufw

ufw

IPV6=no

  

Open ports

sudo ufw default allow outgoing  
sudo ufw default deny incoming  
sudo ufw allow 21/tcp  
sudo ufw allow 22/tcp  
sudo ufw allow 2106/tcp

sudo ufw allow 3306/tcp  
sudo ufw allow 7777/tcp  
sudo ufw allow 19999/tcp  
sudo ufw enable

  

## Configure Network

  
  

### Ubuntu(NetPlan)

  

Configure NetPlan(Wlan/Wifi)

sudo nano /etc/netplan/50-cloud-init.yaml

50-cloud-init.yaml

network:  
version: 2  
ethernets:  
eth0:  
dhcp4: no  
dhcp6: no  
optional: true  
addresses: [192.168.0.13/24]  
gateway4: 192.168.0.1  
nameservers:  
addresses: [8.8.8.8, 8.8.4.4]  
wifis:  
wlan0:  
dhcp4: no  
dhcp6: no  
optional: true  
addresses: [192.168.0.14/24]  
gateway4: 192.168.0.1  
nameservers:  
addresses: [8.8.8.8, 8.8.4.4]  
access-points:  
"SSID":  
password: "PASSWORD"

Test NetPlan

sudo netplan try

Apply Netplan

sudo netplan apply

### Debian(DHCOCD/WPA_SUPPLICANT)

  

Configure DHCPCD(Wlan/Wifi)

sudo nano /etc/dhcpcd.conf

dhcpcd.conf

interface eth0  
static ip_address=192.168.0.13/24  
static routers=192.168.0.1  
static domain_name_servers=8.8.8.8 8.8.4.8  
  
interface wlan0  
static ip_address=192.168.0.14/24  
static routers=192.168.0.1  
static domain_name_servers=8.8.8.8 8.8.4.8

  

Configure WPA_SUPPLICANT(Wlan)

sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

wpa_supplicant.conf

ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev  
update_config=1  
country=ES  
  
network={  
ssid="SSID"  
psk="PASSWORD"  
}

  

Restart networking

sudo systemctl restart networking

### DietPi(NETWORKING/WPA_SUPPLICANT)

  

Configure NETWORKING(Wlan/Wifi)

sudo nano /etc/network/interfaces

interfaces

source interfaces.d/*  
  
auto lo  
iface lo inet loopback  
  
allow-hotplug eth0  
iface eth0 inet static  
address 192.168.0.13  
netmask 255.255.255.0  
gateway 192.168.0.1  
dns-nameservers 8.8.8.8 8.8.4.4  
  
allow-hotplug wlan0  
iface wlan0 inet static  
address 192.168.0.14  
netmask 255.255.255.0  
gateway 192.168.0.1  
dns-nameservers 8.8.8.8 8.8.4.4  
wireless-power off  
wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf

  

Configure WPA_SUPPLICANT(Wlan)

sudo nano /etc/wpa_supplicant/wpa_supplicant.conf

wpa_supplicant.conf

ctrl_interface=DIR=/run/wpa_supplicant GROUP=netdev  
  
update_config=1  
network={  
ssid="SSID"  
scan_ssid=1  
key_mgmt=WPA-PSK  
psk="PASSWORD"  
}

  

Restart networking

sudo systemctl restart networking

# Setup L2JServer - Docker

  
  

## Setup de project

  
  

Clone the docker project

git clone https://gitlab.com/Yeiij/l2jdocker_linux.git

  

Follow README.md instructions.

  

##### Done.

