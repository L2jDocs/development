module.exports = {
  title: 'L2J Server Documentation',
  description: 'Server Emulation done Easy',
  theme: 'yuu',  
  plugins: ['@vuepress/last-updated',
	 '@vuepress/back-to-top',  
	 'vuepress-plugin-smooth-scroll',

  ],
  themeConfig: {
    nav: [{
      text: 'Home',
      link: '/'
    }, {
      text: 'Installation',
      ariaLabel: 'Installation Menu',
      items: [{
        text: 'Getting Started',
        link: '/install/'
      }, {
        text: 'Windows',
        link: '/windows/'
      }, {
        text: 'Linux',
        link: '/linux/'
      }, {
        text: 'macOS',
        link: '/macos/'
      }]
    }, {
      text: 'Documentation',
      ariaLabel: 'Documentation Menu',
      items: [{
        text: 'Documentation',
        link: '/docs/general/'
      }, {
        text: 'Server Design',
        link: '/docs/design/'
      }, {
        text: 'Project Management',
        link: '/docs/managment/'
      }, {
        text: 'Informational Sources',
        link: '/docs/infosource/'
      }, {
        text: 'Server Configuration',
        link: '/docs/config/'
      }, {
        text: 'Geodata',
        link: '/docs/geo/'
      }, {
        text: 'Server Update',
        link: '/docs/update/'
      }, {
        text: 'Server In-Game Commands',
        link: '/docs/ingamecom/'
      }, {
        text: 'Server Features',
        link: '/docs/features/'
      }, {
        text: 'Server Game Features',
        link: '/docs/sgamefeat/'
      }, {
        text: 'Server Customization',
        link: '/docs/custom/'
      }, {
        text: 'Issue Reporting',
        link: '/docs/report/'
      }, {
        text: 'Forking L2J',
        link: '/docs/forking/'
      }, {
        text: 'Developing L2J',
        link: '/docs/dev/'
      }, {
        text: 'Contributing to L2J',
        link: '/docs/contributing/'
      }, {
        text: 'Joining L2J',
        link: '/docs/join/'
      }]
    }, ],
    displayAllHeaders: true,
    smoothScroll: true,
    yuu: {
      defaultDarkTheme: true,
      defaultColorTheme: 'blue',
    },
    sidebar: {
      '/guidelines/': 'auto',
      '/install/': 'auto',
      '/linux/': [{
        title: 'Linux',
        path: '/linux/',
        collapsable: false,
        sidebarDepth: 1,
        children: []
      }, {
        title: 'Ubuntu',
        collapsable: false,
        sidebarDepth: 1,
        children: [
          ['/linux/ubuntu/docker/', 'Docker'],
          ['/linux/ubuntu/git_maven/', 'Git/Maven']
        ]
      }, {
        title: 'CentOS',
        collapsable: false,
        sidebarDepth: 1,
        children: []
      }, {
        title: 'Debian',
        collapsable: false,
        sidebarDepth: 1,
        children: []
      }, ],
      '/windows/': [{
        title: 'Windows 10',
        path: '/windows/',
        collapsable: false,
        sidebarDepth: 1,
        children: []
      }, {
        title: 'Hardware',
        collapsable: false,
        sidebarDepth: 1,
        children: [
          ['/windows/spec/', 'System'],
          ['/windows/net/', 'Network']
        ]
      }, {
        title: 'Software',
        collapsable: false,
        sidebarDepth: 1,
        children: [
          ['/windows/env/', 'Environment'], {
            title: 'OpenJDK',
            collapsable: false,
            sidebarDepth: 1,
            children: [
              ['/windows/adaptjdk/', 'Automatic'],
              ['/windows/openjdk/', 'Manual']
            ]
          },
          ['/windows/git/', 'GIT'],
          ['/windows/mariadb/', 'MariaDB'],
          ['/windows/eclipse/', 'Eclipse [TODO]'], {
            title: 'Database Managment',
            collapsable: false,
            sidebarDepth: 1,
            children: [
              ['/windows/heidi/', 'HeidiSQL [TODO]'],
              ['/windows/navicat/', 'Navicat [TODO]']
            ]
          }
        ]
      }, {
        title: 'Server Installation',
        collapsable: false,
        sidebarDepth: 1,
        children: [{
          title: 'High Five',
          collapsable: false,
          children: [
            ['/windows/highfive_i/', 'Install Manager [TODO]'],
            ['/windows/highfive_s/', 'Snapshot [TODO]'],
            ['/windows/highfive_m/', 'Git/Maven'],
            ['/windows/highfive_e/', 'Eclipse [TODO]']
          ]
        }, {
          title: 'Interlude  [TODO]',
          collapsable: false,
          children: [
            ['/windows/interlude_e/', 'Eclipse [TODO]']
          ]
        }]
      }, {
        title: 'Development Setup',
        collapsable: false,
        sidebarDepth: 1,
        children: [{
          title: 'High Five',
          collapsable: false,
          children: [
            ['/windows/dev_highfive/', 'Intellij IDEA']
          ]
        }]
      }, ]
    },
  }
}