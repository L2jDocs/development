![Network logo](/images/net.png#center)


# Network

---

### [WAN](https://en.wikipedia.org/wiki/Wide_area_network) [IP](https://en.wikipedia.org/wiki/Internet_Protocol) Address 

This is your External IP address. There are numerous services in Internet that will provide you this crucial information. 

For this example we will ask our current IP address from Google: 

![Network](/images/net5.png#center)

This will be later used to configure the client and server

---
### Network Interface Controller ([NIC](https://en.wikipedia.org/wiki/Network_interface_controller))

In order to get NIC information 

-	Open `cmd` and execute the following command: 
```
ipconfig
```
\
Output: 

![Network](/images/net1.png#center)


-	This will provide basic Network information
-	In this specific example we have 3 important things to look out for: 
	1. Current active NIC
	2. IPv4 Adress 
	3. Default Gateway

These will be helpfull for troubleshooting and future server configuration




---

### Network Topology
:::warning Notice
Complex network infracstructure as well as it's configuration will not be covered here.
:::

Networks can be configured phisically and logically. Since this is a very complicated topic, we will only provide the basic information required to setup the L2jServer. 

However, if you want to learn more, look [here](https://en.wikipedia.org/wiki/Network_topology)

Most users will have:

1. Direct Connection 
	- Your machine will recieve an external IP adress from the  [ISP](https://en.wikipedia.org/wiki/Internet_service_provider)  directly.
	- Only firewall rules must be adjusted
	
	
![Network](/images/net3.png#center)

2. Router, provided by the [ISP](https://en.wikipedia.org/wiki/Internet_service_provider)
	- Connection is made thru a gateway.
	- Port forwarding is required.
	- Firewall rules must be adjusted.

![Network](/images/net2.png#center)


3. Router and a **[Network Switch/Second Router]** (Managed by the [ISP](https://en.wikipedia.org/wiki/Internet_service_provider))
	- Port forwarding is required.
	- Firewall rules must be adjusted.
	
![Network](/images/net4.png#center)

:::warning Notice
-	Port forwarding from the [ISP](https://en.wikipedia.org/wiki/Internet_service_provider) side is required. 
-	 This will cause connectivity problems. Since the second router can only be accessed by the [ISP](https://en.wikipedia.org/wiki/Internet_service_provider). 
-	 Usually your [ISP](https://en.wikipedia.org/wiki/Internet_service_provider) can open ports if you ask them politely. Or pay. 

:::
\
\
Easiest way to check this, is by tracing the route from the server machine to a known host, e.g. Google.com

-	To do this open `cmd` and execute the following command: 
```
tracert Google.com
```

Output:

![Network](/images/net6.png#center)

-	In case your network has a router, the first hop will be to the Default Gateway. Second hop should be your WAN subnet. 
-	If it's not, most probably you have a second router in the network. 



:::tip 

If you want to be sure, here is a [list](https://en.wikipedia.org/wiki/Reserved_IP_addresses)    of reserved IP adressed used for Routers (Private Networks):


:::






---

### Port Forwarding
In case you have option `1` - you can skip to the next step.\
In case you have options `2` and `3` - Port Forwarding is mandatory.\
But, since we can't explore every router's configuration and possible scenarios, here is a Beginers Guide on this topic:
\
\
<center>
<iframe width="630" height="350" src="https://www.youtube.com/embed/jfSLxs40sIw" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe></center>\
\
\
After the video, you can start configuring your router:

-	Search you router's model at [PortForward](https://portforward.com/router.htm) (There should be a detailed instruction on how to open ports)
-	Ports: 
```
2106 - This is the Login Server port. 

7777 - This is the Game Server port.

3306 - This is the MariaDB/MySQL default connection port.
```
:::warning Notice
Game Server port can and should be changed, in case you have multiple server instances.\
Login Server port change requires a client modification. 
:::
:::danger Warning
MariaDB port should be forwarded **ONLY** in case have opted for Remote Access (e.g. for an External account managment system)\
If the webserver is located on the same network - port forwarding is not required. 
:::

There are a few ways to check if the ports are opened and properly forwarded: 
1. Online [PortChecker](https://portchecker.co/)
2. Online [CanYouSeeMe](https://www.canyouseeme.org/)
3. [nMap](https://nmap.org/)


***Keep in mind, these services will provide a response only if the Game/Login services are UP and Runing.**


---

### Connection Speed 


Your Connections Speed and latency will heavily affect server's accessibility and playability in case of Live Deployment.\
\
Connection speed can be tested here at [SpeedTest](https://speedtest.net/) or [Fast.com](https://fast.com/)

Latency to different regions can be tested at [Ping test](http://ping-test.net/)
\
\
Approximate network requirements are described  [here](/install/#network-requirements).

---

### Windows Firewall
 

This topic describes how to configure a Windows firewall. Firewall systems help prevent unauthorized access to computer resources. To access an instance of the L2j Server through a firewall, you must configure the firewall on the computer running the Server to allow access. There are many firewall systems available. For information specific to your system, see the firewall documentation.


#### To open a port in the Windows firewall for TCP access

1.  On the  **Start**  menu, type  **WF.msc**, and then open the ![Network](/images/net7.png) console. 
    
2.  In the  **Windows Firewall with Advanced Security**, in the left pane, right-click  **Inbound Rules**, and then click  **New Rule**  in the action pane.
    
3.  In the  **Rule Type**  dialog box, select  **Port**, and then click  **Next**.
    
4.  In the  **Protocol and Ports**  dialog box, select  **TCP**. Select  **Specific local ports**, and then type the port number of the instance, such as  **2106, 7777**  for the default instance. Click  **Next**.
    
5.  In the  **Action**  dialog box, select  **Allow the connection**, and then click  **Next**.
    
6.  In the  **Profile**  dialog box, select any profiles that describe the computer connection environment, and then click  **Next**.
    
7.  In the  **Name**  dialog box, type a name and description for this rule (e.g. L2jServer), and then click  **Finish**.
