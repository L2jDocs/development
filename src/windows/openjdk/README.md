
![Windows logo](/images/openjdk.png#center)


# Download:
---
Download: [OpenJDK 14.0.1 (build 14.0.1+7) ](https://jdk.java.net/archive/)

![JDK](/images/jdk.png#center)

We recommend OpenJDK due to licencing changes from  [Oracle](https://www.oracle.com/technetwork/java/javase/overview/oracle-jdk-faqs.html).



# Installation:

### 	Java
-	Unzip the OpenJDK archive to `C:\Program Files\Java`
	- Should be `C:\Program Files\Java\jdk-14.0.2`
	- This path should be permanent for the server to function

### Environment variables
 
-	Open Start Search (Windows button)
-	Type env and choose "Edit the system environment variables"
-	Click "Environment Variables..."


![env](/images/env.png#center)




-	In "System Variables" find "Path" and click "Edit..."

![env](/images/path.png#center)



-	In "Edit environment variable" click "New" and paste the path `C:\Program Files\Java\jdk-14.0.2\bin`
-	Close all dialogs with "OK"
-	Create a new environment variable JAVA_HOME as `C:\Program Files\Java\jdk-14.0.2`

![env](/images/jp.png#center)

-	To verify that the VM is working, open `cmd` and type:
```
java -version
````

-	Output should be: 
 

![env](/images/javav.png#center)
 
 
 
 
 
