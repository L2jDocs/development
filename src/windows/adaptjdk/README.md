
![AdoptJDK logo](/images/adoptjdk.png#center)


# Download:
---
Build / Download: [OpenJDK 14](https://adoptopenjdk.net/releases.html?variant=openjdk14&jvmVariant=hotspot)

![JDK](/images/adoptjdk1.png#center)\
\
![JDK](/images/adoptjdk2.png#center)

We recommend OpenJDK due to licencing changes from  [Oracle](https://www.oracle.com/technetwork/java/javase/overview/oracle-jdk-faqs.html).



# Installation:


1. Start the installer
2. Select all the components:


![JDK](/images/adoptjdk3.png#center)\
\
\-	To verify that the VM is working, open `cmd` and type:
```
java -version
````

-	Output should be: 
 

![env](/images/javav.png#center)
 
 
 
 
 
