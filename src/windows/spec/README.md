
![System logo](/images/hard.png#center)


# System specifications & relative performance

---
First things first. \
There are a lot of tools on the Internet that will help you get your system information.\
We will use `cmd` 

---
### CPU


-	Open `cmd` and execute the following command: 
```
wmic cpu get caption, deviceid, name, numberofcores, maxclockspeed, status 
```
\
Output: 

![System specs](/images/hard1.png#center)

:::warning Notice 
![System specs](/images/intel.png)   ![System specs](/images/amd.png)   ![System specs](/images/arm.png)  CPUs that are 64-bit capable - can and will run Java. 
:::


:::tip 
Java Virtual Machines greatly benefit from multithreading. The more cores/threads - the better.
:::

---
### RAM


-	Open `cmd` and execute the following command: 
```
systeminfo |find "Physical Memory"
```
\
Output: 

![System specs](/images/hard2.png#center)
 

Phisical memory (RAM) will partially determine the amount of online users your server can handle: 
-	2GB will be enough for dedicated local or LAN usage for a few people
	- Without Geodata
	- Functionality not guaranteed if lower
	- Valid if Lineage II client will not be used from the same machine
	
-	4GB will be good enough for a small server with a 25-50 players range
	- Depending on the storage configuration and Windows demands
	- Valid if Lineage II client will not be used from the same machine	
	
-	8GB is preferred for a medium sized live server with an estimated online of 80-150 players
	- Heavily depends on the network capabilities 
	- Depends on storage configuration
	
-	32GB and higher will be a good starting point for a large scale installation for up to 1000-1500 players 
	- Limited by network capabilities
	- DDR3 and higher
	- ECC is a must
	- Valid only if dedicated
	
:::warning Notice
Numbers provided are aproximate and may vary from server to server
::: 
	
	


---
### Storage


-	Open `cmd` with Eleveated (Administrative) priveleges and execute the following command: 
```
winsat disk -drive C
```
*Note: Change the 'C' to the drive that will hold the Server Installation and the Database*

\
Output: 

![System specs](/images/hard3.png#center)
 
 
*Note: Benchmarks from an NVMe drive*

 

-	Disk Random & Sequential Read/Write are important factors that will directly affect the server's capabilities
	- Higher = Better
-	Latency will determine the responsivness of your server 
	- Lower = Better

:::warning Notice
-	Drive Space is more important when it comes to High-End installations. This is directly related to SSD and NVMe drives and their caching function 
	- However, 2GB and more reserved space for the server installation is a good idea
	
-	Input/output operations per second ([IOPS](https://en.wikipedia.org/wiki/IOPS)) are also crucial for High-End Database installations.
	- Not relevant for HDD and SD cards 

:::

 




