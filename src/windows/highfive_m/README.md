
![HF logo](/images/hf.png#center)


# Maven / Git 

---
### Getting the Source Code using Git


:::warning Notice
-   For this Guide we will use C:\Source as an example
-   This folder will be used to store the Source code and Compiler output
:::

1. Create a folder named **Source** in C:\

2.  Open `cmd` 

3.  Navigate to C:\Source via `cd`. 


4. Execute the following commands: 


```batch
git clone https://bitbucket.org/l2jserver/l2j-server-login.git
git clone https://bitbucket.org/l2jserver/l2j-server-game.git
git clone https://bitbucket.org/l2jserver/l2j-server-datapack.git
```

---
### Building the Server from Source Code

We use **Maven** to build the server files.
Navigate to each of the downloaded folders and execute mvnw command as follows: 

```batch
cd C:\Source\l2j-server-login
mvnw install
cd C:\Source\l2j-server-game
mvnw install
cd C:\Source\l2j-server-datapack
mvnw install
```

This will result - 3 ziped archives in the **/target** subdirectory


---

### Deploying the Server

The deployment process at the moment is simply unzipping the built files. You create a separate directory in the preffered location of the drive. 

For this example - we will create a Folder on the Desktop and name it `Server`, with subdirectories: `Login` & `Game`:

```
Desktop
├── Server
    ├── Login
    └── Game
```

Next:


-	Unzip C:\Source\l2j-server-login\target\ *l2jlogin.zip* to `Login` folder
-	Unzip C:\Source\l2j-server-game\target\ *l2j-server-game-2.6.2.0-SNAPSHOT.zip* to `Game` folder
-	Unzip C:\Source\l2j-server-dp\target\ *l2j-server-datapack-2.6.2.0-SNAPSHOT.zip* to `Game` folder


:::warning Notice
The file names may change with each new version, take that into consideration.
:::


---

### L2J CLI and Database installation

L2J CLI is a tool developed by Zoey76 that provides Database deployment automation.



1.  Download  [L2J CLI](https://l2jserver.com/files/binary/cli/l2jcli-1.0.5.zip)
2.  Run `l2jcli.bat`
3.  Edit and execute the following: 
```batch
db install -sql %Path_to_Server%\login\sql` -u %Database_User% -p %Database_Password% -m FULL -t LOGIN -c -mods
db install -sql %Path_to_Server%\game\sql -u %Database_User% -p %Database_Password% -m FULL -t GAME -c -mods
quit
```

:::warning Notice
-	%Path_to_Server% should be a complete path to you unziped Server. (e.g. C:\Users\John\Desktop\Server)
-	%Database_User% & %Database_Password% is your MariaDB account and password, set during database installation

:::

---


### Connecting Login and Game servers to the Database

Navigate to:

1.  *Path_to_Server*\login\config
2.  Open file `database.properties`
3.  Edit the following lines:


```
# Database User
User = Database_User
# Database Password
Password = Database_Password
```

1.  *Path_to_Server*\game\config
2.  Open file `database.properties`
3.  Edit the following lines:


```
# Database User
User = Database_User
# Database Password
Password = Database_Password
```

---

### Starting the Server

To start the server you need to run two scripts.


1. *Path_to_Server*\login\startLoginServer.bat
2. *Path_to_Server*\game\startGameServer.bat

If you all steps have been done correctly, you should see 2 console windows with Login and Game server's log output

:::warning Notice
-	Game server load times depend on the hardware and may take a few minutes to finish. 
-	If there are no errors, you should see a message that the Game server succesfully connected to Login server
:::

