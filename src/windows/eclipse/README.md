
![Eclipse logo](/images/ecl.png#center)


# Download:
---
Download: [Eclipse IDE / Latest](https://www.eclipse.org/downloads/)
\
\
![Eclipse](/images/ecl1.png#center)

```
Eclipse is an integrated development environment (IDE) used to write/modify/debug the source code. 

Adding custom features or modifying internal game mechanics and settings not 
covered by the configuration files - will require Eclipse.

New users can skip this step, since it's not mandatory for Server installation. 

```

---

# Installation:

//TODO


