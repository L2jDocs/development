
![MariaDB logo](/images/mariadb.png#center)


# Download:
---
Download: [MariaDB Database Server ](https://downloads.mariadb.org/)
\
\
![MariaDB logo](/images/mdb.png#center)

:::warning Notice
Version number may change with time.
:::

---

# Installation:

1.  You will be promoted to install the Database Server and HeidiSQL Database management tool. In case you have opted for another tool, you can deselect HeidiSQL.

![MariaDB](/images/mdb1.png#center)
\
\
2.  This step will require you to configure the database access. 
\
\
![MariaDB](/images/mdb2.png#center)

:::danger Warning
-	Passwords can be the weakest link in a server security deployment. Take great care when you select a password.
-	`root` password should be secure, especially in case of a live production server.
-	Remote `root` access should **NOT** be enabled, unless you are **absolutely** sure what you are doing.
:::

:::warning Notice
-	L2jserver uses a separate database user with it's own password and permissions. 
-	UTF8 can be set as the default character set, yet every database can be configured independently for regional compatibility.
:::
\
\
3.  Here you can select the service port. 
\
\
![MariaDB](/images/mdb3.png#center)


1.	Install as service
	-	Opting out will force you to manually start the Database each time the system is restarted. 

2.	TCP port
	- If you don't know what you're doing - leave it like that. 
	- Changing the default port will force you to edit l2jserver configuration files to avoid connection failures.


3.	Default Buffer pool size:
	- If you don't know what you're doing - leave it like that. 
	- If you wish to tweak for a more performant setup - set size from 20% to 70% of available RAM.

::: details [Spoiler] Rule of thumb for tuning
-	Start with released copy of my.cnf / my.ini.
-	Change key_buffer_size and innodb_buffer_pool_size according to engine usage and RAM.
-	Slow queries can usually be 'fixed' via indexes, schema changes, or SELECT changes, not by tuning.
-	Don't get carried away with the query cache until you understand what it can and cannot do.
-	Don't change anything else unless you run into trouble (eg, max connections).
-	Be sure the changes are under the [mysqld] section, not some other section.
-	The 20%/70% assumes you have at least 4GB of RAM. If you have a tiny antique, or a tiny VM, then those percentages are too high
:::
\
\
\
4. Check the result

You can check if the Database is online by running `cmd` with Eleveated (Administrative) priveleges and the following command:
```
netstat -ano | findStr "3306"
```


The output should be:

![MariaDB](/images/mdbc.png#center)




