
![Windows logo](/images/win.png#center)


# Environment

---
1.	**Windows should be updated to the latest version.**
	- This is done for compatibility reasons, yet not mandatory.

::: warning Warning
32-bit Windows versions are not recommended.
:::

\
\
2.	 **Database port should be available for internal use.**

You can check this by running `cmd` with Eleveated (Administrative) priveleges and the following command:
```
netstat -ano | findStr "3306"
```
If there is no output to that command, then the port required for the Database is free.

::: tip Other DB installations 
In case that you already own an L2J server or other projects and another instance of the Database is already installed, then there is no need to remove it. L2J is still compatible with MySQL 8.0, however we strongly recommend using MariaDB.
:::

\
\
3.	**No other versions of Java are present.**

L2J uses OpenJDK 14. Other versions are **not compatible**.

To check this, open `cmd` and type:
```
java -version
```

If the system returns an error, most likely no Java is installed. 

Otherwise, please remove any previous Java VMs from 'Apps and features' menu or directly from Program Files and **restart the system**.


 
