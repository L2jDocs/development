![](/images/dev_highfive/intellij.png#center)


# IntelliJ Development setup

---
### 1. Clone the repos

![](/images/dev_highfive/0.png#center#center)

![](/images/dev_highfive/1.png#center)

#### **Repeat with all the repos**
---


### 2. Configure the project

![](/images/dev_highfive/2.png#center)

![](/images/dev_highfive/3.png#center)

---

### 3. Group all modules in one project

![](/images/dev_highfive/4.png#center)

![](/images/dev_highfive/5.png#center)

![](/images/dev_highfive/6.png#center)

![](/images/dev_highfive/7.png#center)

---

### 4. Configure the Run [CLI, Login, Game]

![](/images/dev_highfive/8.png#center)

---

### LOGIN

![](/images/dev_highfive/9.png#center)

---

### CLI

![](/images/dev_highfive/10.png#center)

---


### GAME

![](/images/dev_highfive/11.png#center)

![](/images/dev_highfive/12.png#center)

---

### 5. Run these projects

### LOGIN

![](/images/dev_highfive/13.png#center)

![](/images/dev_highfive/14.png#center)

### GAME

![](/images/dev_highfive/15.png#center)

![](/images/dev_highfive/16.png#center)