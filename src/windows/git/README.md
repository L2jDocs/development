
![GIT logo](/images/git.png#center)


# Download:
---
Download:  [Git  (2.28.0) 64-bit version ](https://git-scm.com/download/win)

![GIT](/images/gitdl.png#center)

 
 
:::warning Notice
We recommend using the Default installation options for new users

Below is a step by step guide. Just in case.

:::




---

# Installation:



 
1. Git Large File Support is recommended 

![GIT Install](/images/git1.png#center)
 
2. You can use any text editor in this scenario. For example purposes: Notepad++ 

![GIT Install](/images/git2.png#center)

3. Leave it like this.

![GIT Install](/images/git3.png#center)
 
4. Leave it like this.

![GIT Install](/images/git4.png#center)

5. This is optional.

![GIT Install](/images/git5.png#center)
 
6. Leave it like this.

![GIT Install](/images/git6.png#center)

7. Leave it like this.

![GIT Install](/images/git7.png#center)
 
8. Leave it like this.

![GIT Install](/images/git8.png#center)

9. Leave it like this.

![GIT Install](/images/git9.png#center)








---

-	The output console windows should look like this: 

![GIT Install](/images/git11.png#center)
 